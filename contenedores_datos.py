

numeros = [10,20,30,40,10,10,20,30,40,50,60]
print(numeros)
#Castear/convertir
conjuntos_numeros = set(numeros)
print(conjuntos_numeros)
numeros = list(conjuntos_numeros)
print(numeros)
#Ordenar los números en la lista
numeros.sort(reverse=True)
print(numeros)

tupla_numeros = tuple(conjuntos_numeros)
print(tupla_numeros)
tamanio = len(conjuntos_numeros)
print('Tamaño conjunto-> ', tamanio)

diccionario = {
    'nombre': 'Juan',
    'apellido': 'Castro',
    'edad': 25,
    'celular': '123456789'
}

conjunto_persona_keys = set(diccionario.keys())
print(conjunto_persona_keys)

conjunto_persona_values = set(diccionario.values())
print(conjunto_persona_values)


mensaje = 'Hola mundo desde Misión Tic 2022 - UTP'
conjunto_caracteres = set(mensaje)
print(conjunto_caracteres)

lista_de_claves = list(diccionario.keys())
print(lista_de_claves)